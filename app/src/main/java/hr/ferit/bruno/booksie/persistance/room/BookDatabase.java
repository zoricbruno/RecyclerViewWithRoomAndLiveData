package hr.ferit.bruno.booksie.persistance.room;

import android.app.Application;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;

import hr.ferit.bruno.booksie.model.Book;

/**
 * Created by Stray on 3.4.2018..
 */

@Database(entities = {Book.class}, version = 1)
public abstract class BookDatabase extends RoomDatabase {

	private static BookDatabase sInstance;
	private static final String DATABASE_NAME = "books.db";

	public static BookDatabase getInstance(Application application){
		if(sInstance == null){
			sInstance = Room.databaseBuilder(
					application.getApplicationContext(),
					BookDatabase.class,
					DATABASE_NAME
			).build();
		}
		return sInstance;
	}

	// Database access objects:
	public abstract BookDao bookDao();
}
