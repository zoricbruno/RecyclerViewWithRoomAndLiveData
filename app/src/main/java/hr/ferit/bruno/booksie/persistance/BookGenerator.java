package hr.ferit.bruno.booksie.persistance;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import hr.ferit.bruno.booksie.model.Book;

public class BookGenerator {

    private static final Random generator = new Random();

    public static Book generate(){
        int id = generator.nextInt(); // Does not guarantee uniqueness
        String title = TITLES[generator.nextInt(TITLES.length)];
        String author = AUTHORS[generator.nextInt(AUTHORS.length)];
        String coverUrl = URL;
        return new Book(id, title, author, coverUrl);
    }

    public static List<Book> generate(int count){
        List<Book> books = new ArrayList<Book>();
        for (int i=0; i<count; i++){
            books.add(BookGenerator.generate());
        }
        return books;
    }

    private static final String URL = "https://picsum.photos/90/120/?random";
    private static final String[] TITLES = new String[]{
            "Priest Of The River",
            "Butterfly Of The Gods",
            "Children Without Desire",
            "Spiders Of The Banished",
            "Hunters And Rats",
            "Heirs And Doctors",
            "Misfortune Without A Conscience",
            "Argument Of The North",
            "Rescue At The Elements",
            "Whispers Of The South",
            "Officer Of The Past",
            "Dog In My Garden",
            "Girls Of An Asteroid",
            "Mice Of The Swamp",
            "Witches And Rebels",
            "Women And Mice",
            "Prediction In My City",
            "Question Of The River",
            "Help Of The South",
            "Moral Of The Cliffs"
    };
    private static final String[] AUTHORS = new String[]{
            "Steve Bell",
            "Edmund Griffith",
            "Billy Stephens",
            "George Miles",
            "Jon Rhodes",
            "Henry Pace",
            "Lee England",
            "Zachary Hall",
            "Owen Ray",
            "Sergio Kirk",
            "Dorothea English",
            "Marjorie Hatfield",
            "Barbara Leon",
            "Janet Baker",
            "Nicole Larsen",
            "Doris Shields",
            "Sherri Lopez",
            "Alisha Flynn",
            "Brenda McFarland",
            "Olivia Lambert"
    };

}
