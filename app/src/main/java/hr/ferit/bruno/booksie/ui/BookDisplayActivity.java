package hr.ferit.bruno.booksie.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.bruno.booksie.R;
import hr.ferit.bruno.booksie.model.Book;
import hr.ferit.bruno.booksie.viewmodel.BookListViewModel;

public class BookDisplayActivity extends AppCompatActivity {

    @BindView(R.id.rvBooks) RecyclerView rvBooks;
    @BindView(R.id.fabAddBook) FloatingActionButton fabAddBook;

    private BookListViewModel mBookListViewModel;

    private BookClickCallback mOnBookClickListener = new BookClickCallback() {
        @Override
        public void onClick(Book book) {
            String message = book.getTitle();
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        }

        @Override
        public boolean onLongClick(Book book) {
            mBookListViewModel.deleteBook(book);
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books_display);
        ButterKnife.bind(this);
        mBookListViewModel = ViewModelProviders.of(this)
                .get(BookListViewModel.class);
        this.setUpRecyclerView();

    }

    private void setUpRecyclerView() {
        LinearLayoutManager linearLayout =
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        DividerItemDecoration divider =
                new DividerItemDecoration(this, linearLayout.getOrientation());

        BookAdapter adapter = new BookAdapter(
                new ArrayList<Book>(), mOnBookClickListener);

        rvBooks.setLayoutManager(linearLayout);
        rvBooks.addItemDecoration(divider);
        rvBooks.setAdapter(adapter);

        mBookListViewModel.getBookList().observe(this, new Observer<List<Book>>() {
            @Override
            public void onChanged(@Nullable List<Book> books) {
                ((BookAdapter)(rvBooks.getAdapter())).refreshData(books);
            }
        });
    }

    @OnClick(R.id.fabAddBook)
    public void addBook(){
        mBookListViewModel.insertBook();
    }
}
